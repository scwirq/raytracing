#include <iostream>
#include <opencv2/opencv.hpp>
#include <vector>
#include <cmath>
#include <memory>
#include <tuple>

#define EPSILON 1e-6

//全部この中に書くのは良いとはいえないけど、まあやってしまおう

struct Vector{
	float x, y, z;

	Vector(){}
	Vector(float a, float b, float c) : x(a), y(b), z(c) {}
	Vector( const Vector& r ) : x(r.x), y(r.y), z(r.z) {}

	float dot( Vector r ) const {
		return x * r.x + y * r.y + z * r.z;
	}

	float norm() const {
		return sqrt(dot( *this ));
	}

	const Vector outer( const Vector r ) const {
		return Vector( y*r.z - z*r.y, z*r.x - x*r.z, x*r.y - y*r.x );
	}

	const Vector operator+( const Vector r ) const {
		return Vector( x + r.x, y + r.y, z + r.z );
	}

	const Vector operator-( const Vector r ) const {
		return Vector( x - r.x, y - r.y, z - r.z );
	}


	const Vector operator*( const float s ) const {
		return Vector( x * s, y * s, z * s );
	}

	const Vector operator/( const float s ) const {
		return Vector( x / s, y / s, z / s );
	}

	float& operator[]( const unsigned i ) {
		return *((float*)(this) + i);
	}

	const float& operator[]( const unsigned i ) const {
		return *((float*)(this) + i);
	}
	friend const Vector operator*( const float s, const Vector r ) {
		return Vector( r.x * s, r.y * s, r.z * s );
	}
};



union RGB{
	RGB(){}
	RGB( int n ) { rgb = n; };
	RGB( unsigned char a[4] ) { 
		for ( int i = 0; i < 4; ++i ) {
			m[i] = a[i];
		}
	}
	RGB( unsigned char a, unsigned char b, unsigned char c ) {
		m[0] = a;
		m[1] = b;
		m[2] = c;
	}
	const RGB operator*( const float s ) const {
		unsigned char a[4];
		for ( int i = 0; i < 4; ++i ) {
			a[i] = m[i] * s;
		}
		return RGB( a );
	}

	friend const RGB operator*( const float s, const RGB rgb ) {
		unsigned char a[4];
		for ( int i = 0; i < 4; ++i ) {
			a[i] = rgb.m[i] * s;
		}
		return RGB( a );
	}
	unsigned rgb;
	struct{
		unsigned char m[4];
	};
	struct{
		unsigned char a, r, g, b;
	};
};

enum Kind{
	PLANE,
	SPHERE,
};

struct Object{
	Kind kind;
	Vector pos;
	virtual std::tuple<bool, float>  getCollisionPoint( const Vector &u, const Vector &v ) = 0;
	Vector (*absorb)( const Vector &r ); //吸収率
	float (*transmittance)( const Vector &r ); //透過率
	float (*reflectance)( const Vector &r ); //反射率
	float (*refractive)( const Vector &r ); //屈折率
};

/*struct Box : Object{
	Vector nh;
	Vector nw;
	float h;
	float w;
	float a;
};*/

struct Plane : Object{
	Vector w;
	Vector h;

	std::tuple<bool, float>  getCollisionPoint( const Vector &u, const Vector &v )
	{
		Vector n = w.outer(h);
		n = n / n.norm();
		float t = n.dot(pos - v) / (n.dot(u));
		float b = h.dot( pos - u*t - v ) / (h.dot(h));
		float a = w.dot( pos - u*t - v ) / (w.dot(w));
		bool suc = true;
		if ( fabs(b) > 1 || fabs(a) > 1 || t < EPSILON ) suc = false;

		return std::tuple<bool, float>( suc, t );
	}
	//float h;
	//float w;
};

struct Sphere : Object
{
	float r;
	std::tuple<bool, float>  getCollisionPoint( const Vector &u, const Vector &v )
	{
		float a = u.dot(u);
		float b = 2 * u.dot( v-pos );
		float c = (v-pos).dot(v-pos) - r*r;
		float D = b*b - 4*a*c;
		bool suc = true;
		if ( D < EPSILON ) suc = false;
		float t;
		if ( suc ) {
			t = (-b - sqrt(D)) / (2*a);
			if ( t < EPSILON ) t = (-b + sqrt(D)) / (2*a);
			//if ( t < EPSILON ) t = -2*c/(b+sqrt(D));;
			if ( t < EPSILON ) {
				t = 0;
				suc = false;
			}
		} else {
			t = 0;
		}

		return std::tuple<bool, float>( suc, t );
	}
};

struct Eye{
	Vector pos;
	Vector tar;
	Vector nor;
};

struct Display{
	Display( float a, float W, float H ) {
		alpha = a;
		aspect_ratio = H / W;
		e = 1 / tan(alpha / 2);
		beta = 2 * atan( aspect_ratio / e );
	}
	float alpha;
	float beta;
	float aspect_ratio;
	float e;
};

struct Light{
};

struct PointLight : Light{
	Vector pos;
	RGB color;
	float P;
	float kc, kl, kq;
};

RGB calc( Vector u, Vector v, const std::vector<std::unique_ptr<Object>> &obj, const std::vector<std::unique_ptr<Light>> &light, int m ) //ut + v
{
	if ( m > 2 ) return 0;
	RGB rgb;
	rgb = RGB(157, 204, 220) * 0.5;
	float t = 1e10; //無限のかなた
	Vector p;
	Vector n;
	float eta_L = 1;
	float eta_T = 1;

	float trans = 1;
	float ref = 1;
	Vector abso = {1, 1, 1};
	for ( unsigned i = 0; i < obj.size(); ++i ) {
		auto d = obj[i]->getCollisionPoint( u, v );

		if ( std::get<0>(d) == false ) continue; //衝突してない
		if ( std::get<1>(d) > t ) continue;
		t = std::get<1>(d);
		p = u*t + v;

		Vector q = p - obj[i]->pos;

		trans = obj[i]->transmittance(q);
		abso = obj[i]->absorb(q);
		ref = obj[i]->reflectance(q);

		switch( obj[i]->kind ) {
			case SPHERE:{
				const Sphere &data = *((Sphere*)obj[i].get());
				if ( u.dot(p - data.pos) > 0 ) {
					n = p - data.pos;
					n = n / n.norm();
					eta_L = 1;
					eta_T = data.refractive(q); //本当は回転の寄与を考える必要があり
				} else {
					n = data.pos - p;
					n = n / n.norm();
					eta_T = 1;
					eta_L = data.refractive(q); //本当は回転の寄与を考える必要があり
				}
				break;
			}

			case PLANE:{
				const Plane &data = *((Plane*)obj[i].get());
				n = data.w.outer(data.h);
				n = n / n.norm();
				eta_L = 1;
				eta_T = data.refractive(q); //本当は回転の寄与を考える必要があり
				break;
			}
		}
	}

	Vector R, T;
	if ( t != 1e10 ) {
		rgb = 0;
		RGB trgb;
		R = u - n * 2*(n.dot(u));
		float D = 1 - (eta_L/eta_T)*(eta_L/eta_T)*(1 - n.dot(u)*n.dot(u));
		if ( D >= 0 ) {
			T = n*(-eta_L / eta_T * n.dot(u) - sqrt(D)) + u * eta_L/eta_T;
			trgb = calc( T, p, obj, light, m+1 ) * trans;
			for ( int i = 0; i < 3; ++i ) {
				rgb.m[i] += trgb.m[i];
			}
		} else {
			ref += trans;
		}
		auto rrgb = calc( R, p, obj, light, m+1 ) * ref;
		for ( int i = 0; i < 3; ++i ) {
			rgb.m[i] += rrgb.m[i];
		}

		for ( int j = 0; j < light.size(); ++j ) {
			bool flag = false;
			auto l = *((PointLight*)light[j].get());
			for ( int i = 0; i < obj.size(); ++i ) {
				auto data = obj[i]->getCollisionPoint(l.pos - p, p);
				flag |= std::get<0>( data );
			}
			if ( flag == false ) {
				float power = 1/(l.kc + (l.pos-p).norm()*l.kl + (l.pos-p).dot(l.pos-p)*l.kq);
				for ( int k = 0; k < 3; ++k ) {
					rgb.m[k] += l.P * power * l.color.m[k] * abso[k];
					//printf( "%f\t%d\t%f\t%d", power, l.color.m[k], abso[k], rgb.m[k] );
					//getchar();
				}
			}
		}
	}

	return rgb;

}

int main(void)
{
	//const float L = 1;
	const int W = 960;
	const int H = 680;
	//int fd = serialOpen("/dev/tttyAMA0", 9600);
	
	std::vector<std::unique_ptr<Light>> light;
	light.push_back(std::unique_ptr<Light>(new PointLight()));
	((PointLight*)light.back().get())->pos = Vector( 0, -1, 0 ) * 7.5;
	((PointLight*)light.back().get())->color = 0xffffffff;
	((PointLight*)light.back().get())->P = 1;
	((PointLight*)light.back().get())->kc = 1;
	((PointLight*)light.back().get())->kl = 0.0;
	((PointLight*)light.back().get())->kq = 0.0;

	Eye eye = { {0, 0, -10}, {0, 0, 1}, {0, 1 , 0} };
	Display display( 60 / 180.0 * M_PI, W, H );
	std::vector<std::unique_ptr<Object>> obj;
/*
	obj.push_back(std::unique_ptr<Object>(new Plane()));
	((Plane*)obj.back().get())->w = Vector(1, 0, 0) * 10;
	((Plane*)obj.back().get())->h = Vector(0, 1, 0) * 10;
	obj.back()->absorb = []( const Vector &r ) -> Vector { return Vector(157, 204, 224)  / 255; };
	obj.back()->transmittance = []( const Vector &r ) -> float { return 0.0; };
	obj.back()->reflectance = []( const Vector &r ) -> float { return 0.0; };
	obj.back()->refractive = [](const Vector &r) -> float { return 0; };
	obj.back()->pos = Vector(0, 0, 10);
	obj.back()->kind = PLANE;
*/

	obj.push_back(std::unique_ptr<Object>(new Plane()));
	((Plane*)obj.back().get())->w = Vector(1, 0, 0) * 10;
	((Plane*)obj.back().get())->h = Vector(0, 0, 1) * 10;
	obj.back()->absorb = []( const Vector &r ) -> Vector { return ((int)r.x + (int)r.z) % 2 ? Vector(0.0, 0.0, 0.0) : Vector(1, 1, 1)*0.5; };
	obj.back()->transmittance = []( const Vector &r ) -> float { return 0.2; };
	obj.back()->reflectance = []( const Vector &r ) -> float { return 0.4; };
	obj.back()->refractive = [](const Vector &r) -> float { return 1.2; };
	obj.back()->pos = Vector(0, 1, 0);
	obj.back()->kind = PLANE;

	obj.push_back(std::unique_ptr<Object>(new Sphere()));
	((Sphere*)obj.back().get())->r = 0.5;
	obj.back()->absorb = []( const Vector &r ) -> Vector { return /*Vector(218, 165, 32) / 255*/ Vector(0,0,0)*0.9; };
	obj.back()->transmittance = []( const Vector &r ) -> float { return 0.5; };
	obj.back()->reflectance = []( const Vector &r ) -> float { return 0.0; };
	obj.back()->refractive = [](const Vector &r) -> float { return 1.2; };
	obj.back()->pos = Vector(0, 0, 0);
	
	obj.back()->kind = SPHERE;

	cv::Mat img(cv::Size(W, H), CV_8UC3);

	for ( int h = 0; h < 10; ++h ) {
		((PointLight*)light.back().get())->pos = Vector( 0, -1, -2 ) * h;
	for ( int y = 0; y < H; ++y ) {
		for ( int x = 0; x < W; ++x ) {
			Vector target = (eye.pos + eye.tar * display.e) + ( (eye.tar.outer(eye.nor) * display.e * tan(display.alpha/2) * ((float)x - W/2) / W)
					+ (eye.nor * display.e * tan(display.beta/2) * ((float)y - H/2) / H) );
			auto v = calc(target - eye.pos, eye.pos, obj, light, 0);
			for ( int i = 0; i < 3; ++i ) {
				img.at<cv::Vec3b>(y, x)[i] = v.m[2-i];
				//std::cout << (int)img.at<cv::Vec3b>(y, x)[i] << std::endl;
				//getchar();
			}
		}
	}
	cv::imshow( "raytracing", img );
	cv::waitKey(0);
	}


	return 0;
}
