TARGET = raytracing

CC = clang
CXX = clang++
CXXFLAG = -std=c++14 -O2 -mfloat-abi=hard -mfpu=vfp

SRC = main.cpp
OBJ = main.o
LIB = -L/usr/lib/arm-linux-gnueabihf -lopencv_core -lopencv_highgui


$(TARGET): $(OBJ)
	     $(CXX) $(LIB) -o $(TARGET) $^

.cpp.o:
	$(CXX) $(CXXFLAG) -c $<
